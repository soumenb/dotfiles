# First Steps

## Get Chrome

* Save the [installer](https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb).  
* Run the installer with `sudo apt install ~/Downloads/google-chrome-stable_current_amd64.deb` 

## VSCode
* Get the .deb installer from [here](https://code.visualstudio.com/download#)
* Run the installer with `sudo apt install ~/Downloads/code_1.75.1-1675893397_amd64.deb` 

## Gather all the apt install commands
* `sudo apt install cinnamon-desktop-environment git konsole meld python3 stow tmux tree vim-gtk3 zsh`

### Install Cinnamon
* `sudo apt install cinnamon-desktop-environment`

### Latest Python Versions
* `sudo apt install python3`

### Vim, GVim
* `sudo apt install vim-gtk3`

### Stow, Tmux, Zsh
* `sudo apt install stow tmux zsh`

## Fonts - Ligature, FiraCode Mono Font with Ligatures
* Download and extract the Mono fonts from this [zip](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/FiraCode.zip)
* Use Konsole as preferred terminal, and set the preferred font to the downloaded `NerdFont Fira Code`.

## TMux
* `git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm`


## PowerLevel10K
* Manually install PowerLevel10k:  `git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git ~/Utilities/powerlevel10k`




## Clone the DotFiles repository
* `git clone https://gitlab.com/soumenb/dotfiles.git ~/DotFiles`

## Stow the contents
* '~/DotFiles/bin/bin/firstRun`


## Zsh, PowerLevel10k
* Check the current shell isnt't already zsh: `echo $SHELL`
* Set the shell `chsh -s $(which zsh)`

## Setup Vim and Plugins
* `git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
* `vim +PluginInstall +qall`

## Plugins for TMux
* Run `tmux new -A -s Testing`
* Press `Ctrl`+`A`+`I` to install the TMux plugins.


## Generate an SSH Key
* Used to securely connect to remote machines, and also for things like git.
* Keys have to be stored in `~/.ssh` directory.  You rarely need to manually edit files in this directory, let the tools do it automatically.
* You should use a keyphrase; a phrase that unlocks the key the first time that you use it in a session.  Think of a phrase, but keep it to a few words, and decide on upper/lower case, with/without spaces beforehand. `decoratedscarecrow` is a decent passphrase.
* Run `ssh-keygen -t ecdsa -b 521`
* Use the defaults suggested, but add the passphrase that you decided.
* Copy the public key as required, never share the private key. 




