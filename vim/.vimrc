" Enable modern Vim features not compatible with Vi spec.
set nocompatible
set t_Co=256
set encoding=utf-8
set visualbell

" Show line numbers
set number
" Show relative line numbers (useful for editing with linewise movements)
set relativenumber

" Highlight the search term when you search for it, but don't highlight
" just because we've sourced vimrc
set hlsearch
let @/ = ""

" Expand tabs into spaces on insert
set expandtab
" Set tabstop to be size 2  ( :-(  2 is the standard at Google)
set ts=2
" Set shiftwidth to 2 to match tabstop
set sw=2

" Explicitly set the Leader to comma. You can use '\' (the default),
" or anything else (some people like ';').
let mapleader=','

" Show when the leader key is active
set showcmd

" Reload the vimrc
nnoremap <leader>RR :source ~/.vimrc<CR>

" Clear the search register
nnoremap <leader>cs :let @/=''<CR>

" Automatically change the working path to the path of the current file
autocmd BufNewFile,BufEnter * silent! lcd %:p:h

" " Automatically format markdown files on save.
" autocmd FileType markdown AutoFormatBuffer mdformat

" use » to mark Tabs and ° to mark trailing whitespace. This is a
" non-obtrusive way to mark these special characters.
set list listchars=tab:»\ ,trail:°

" By default, it looks up man pages for the word under the cursor, which isn't
" very useful, so we map it to something else.
nnoremap <s-k> <CR>

" All of your plugins must be added before the following line.
" See go/vim-plugin-manager if you need help picking a plugin manager and
" setting it up.
set rtp+=~/.vim/bundle/Vundle.vim
if isdirectory(expand('$HOME/.vim/bundle/Vundle.vim'))

  call vundle#begin()
  " Let Vundle manage Vundle, required

  Plugin 'VundleVim/vundle.vim'
  " Install plugins that come from github.  Once Vundle is installed, these can be
  " installed with :PluginInstall
  "Specify plugins to install here with
  "Plugin 'github_user.github_repo'

  " Airline plugin
  Plugin 'vim-airline/vim-airline'
  Plugin 'vim-airline/vim-airline-themes'

  " Tmuxline plugin
  Plugin 'edkolev/tmuxline.vim'

  " Solarized colorscheme
  Plugin 'altercation/vim-colors-solarized'

  call vundle#end()
else
  echomsg 'Vundle is not installed. You can install Vundle from'
      \ 'https://github.com/VundleVim/Vundle.vim'
endif

" air-line
let g:airline_powerline_fonts = 1
let g:airline_solarized_bg = 'dark'
let g:airline_theme = 'solarized'

" Disable tmuxline extension until the next time the snapshot is to be
" re-created.
let g:airline#extensions#tmuxline#enabled = 1

" Enable file type based indent configuration and syntax highlighting.
" Note that when code is pasted via the terminal, vim by default does not detect
" that the code is pasted (as opposed to when using vim's paste mappings), which
" leads to incorrect indentation when indent mode is on.
" To work around this, use ":set paste" / ":set nopaste" to toggle paste mode.
" You can also use a plugin to:
" - enter insert mode with paste (https://github.com/tpope/vim-unimpaired)
" - auto-detect pasting (https://github.com/ConradIrwin/vim-bracketed-paste)
filetype plugin indent on
syntax on

" Use a dark background
set background=dark

" Use the solarized colorscheme, from https://github.com/altercation/vim-colors-solarized
colorscheme solarized

" Set the font if this is a gui
if has('gui_running')
  " Linux GUI
  if has("gui_gtk2") || has("gui_gtk3")
    set guifont=FiraCode\ Nerd\ Font\ Mono\ 14
    set guioptions-=T
    set lines=30
    set columns=100
  else
    echo "Unknown GUI System!!!"
  endif
else
  "Terminal Vim
  " echo "Vim is running in a terminal"
endif
