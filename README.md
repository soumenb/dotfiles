# Configuration for Zsh, Tmux, Vim, with Version Controlled Dotfiles

## Get the basic apps installed
Install git, vim, gvim, meld, tree, tmux, stow and zsh

```sh
sudo apt install git vim vim-gtk3 meld tree tmux stow zsh
```
## Install Firacode, a monospaced programming font with ligatures

Download and install [Firacode, patched for Nerd Fonts](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip).  Fira Code is an awesome font, supporting ligatures, and the Nerd Font team has 

1. Download the zip file and extract to a local directory.
```sh
mkdir ~/tmpFira
cd ~/tmpFira
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
unzip FiraCode.zip
```
2. Create a local fonts folder, if it doesn't exist.
```sh
mkdir ~/.local/share/fonts
```

3. Move the font files to the local font folder, rebuild the font cache and confirm the presence of FiraCode font.
```sh
mv Fira\ Code\ Bold\ Nerd\ Font\ Complete.ttf   ~/.local/share/fonts/
mv Fira\ Code\ Light\ Nerd\ Font\ Complete.ttf   ~/.local/share/fonts/
mv Fira\ Code\ Medium\ Nerd\ Font\ Complete.ttf  ~/.local/share/fonts/
mv Fira\ Code\ Regular\ Nerd\ Font\ Complete.ttf ~/.local/share/fonts/
mv Fira\ Code\ Retina\ Nerd\ Font\ Complete.ttf  ~/.local/share/fonts/
fc-cache -f -v
fc-list | grep Fira
```

4. Finally, configure Terminal to use FirCode as the preferred font, and confirm that the ligatures are present.  I personally set the font size to 14pt.  To test the ligatures, try typing ... or >= in the terminal, they should be replaced by corresponding glyphs.

## Create your local DotFiles repository
```git clone``` this repository to start with, as follows:
```sh
mkdir ~/DotFiles
git clone https://gitlab.com/soumenb/dotfiles.git ~/DotFiles
```

## Install Vundle to manage Vim Plugins
Install from the [Vundles Github paae](https://github.com/VundleVim/Vundle.vim)
```sh
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
```

## Install PowerLevel10K
Next, create a Utilities directory, where you will clone PowerLevel10K.
```sh
mkdir ~/Utilities
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/Utilities/powerlevel10k
```
echo 'source ~/Utilities/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc

## Set the Configuration files for Zsh and PowerLevel10K
Now, stow the configuration files for zsh and powerlevel10k in the appropriate place. 
```sh
stow --dir=$HOME/DotFiles --target=$HOME -S zsh
stow --dir=$HOME/DotFiles --target=$HOME -S powerlevel10k
```

## Enable Zsh
First, enable zsh:
```sh
chsh -s $(which zsh)
```

Finally, logout and log in again, to see the changes.

# Finally, Tmux, your terminal multiplexer
Tmux has been already installed above.  Now just link in the config, clone TPM, for managing plugins and then install the plugins.
```sh
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
stow --dir=$HOME/DotFiles --target=$HOME tmux
stow --dir=$HOME/DotFiles --target=$HOME bin
stow --dir=$HOME/DotFiles --target=$HOME vim
stow --dir=$HOME/DotFiles --target=$HOME git
tmux new -A -s "Testing"
```
Press ```Ctrl```+```A``` +```I``` ('*leader*' and **I**mport) to load the plugins in TMUX.  WHen you have a configuration that you want to preserve, remember to save the configuration using ```Ctrl```+```A``` then ```Ctrl```+```S```  ('*leader*' and **S**ave).  The configuration is reloaded when tmux restarts, or you can call manually, using ```Ctrl```+```A``` then ```Ctrl```+```R``` ('*leader*' and **R**estore).

Remember to edit your vim config file for your own name and email.






